//Mercadopago.setPublishableKey("TEST-d026d393-8981-47f0-bea9-f8f7b8c0d2f7");
Mercadopago.setPublishableKey( mp_public_key );

Mercadopago.getIdentificationTypes();

var installments, selectedInstallment, flagSubmit, popup

$(function(){
    
    function addEvent(to, type, fn){ 
        if(document.addEventListener){
            to.addEventListener(type, fn, false);
        } else if(document.attachEvent){
            to.attachEvent('on'+type, fn);
        } else {
            to['on'+type] = fn;
        }  
    }; 
    
    addEvent(document.querySelector('#cardNumber'), 'keyup', guessingPaymentMethod);
    addEvent(document.querySelector('#cardNumber'), 'change', guessingPaymentMethod);
    addEvent(document.querySelector('#installments'), 'change', onChangeInstallment);
    addEvent(document.querySelector('#monto'), 'change', onChangeMonto)

    function onChangeMonto(event) {
        const monto = document.getElementById("monto");
        if( monto.value )
            guessingPaymentMethod(event);
    };

    function onChangeInstallment(event) {
        if( $("#installments").val() != "" )
        {   
            selectedInstallment = installments.payer_costs.filter(x => x.installments == $("#installments").val())[0]
            $('#issuer').html(selectedInstallment.installment_rate_collector[0])
            let cuota = selectedInstallment.recommended_message
            let sumaCuotas = cuota.substring(cuota.indexOf("(") + 1,cuota.indexOf(")"))
            $("#montoTotal").html(sumaCuotas)
            if(selectedInstallment.labels[0].indexOf("recommended_installment") == -1)
                $("#tea").html(selectedInstallment.labels[0])
            else
                $("#tea").html(selectedInstallment.labels[1])
        }
    };

    function getBin() {
        const cardnumber = document.getElementById("cardNumber");
        let _cardnumber = cardnumber.value.replace(/\s/g, '');
        return _cardnumber.substring(0,6);
    };

    
    function setInstallmentInfo(status, response) {
        var selectorInstallments = document.querySelector("#installments"),
        fragment = document.createDocumentFragment();
        selectorInstallments.options.length = 0;

        if (response.length > 0) {
            var option = new Option("Cantidad de cuotas", ''),
            payerCosts = response[0].payer_costs;
            installments = response[0];
            let cardType
            cardType = installments.payment_method_id
            if( installments.payment_method_id == "master" ) {
                cardType = "mastercard"
            }
            $("#logoTarjeta").attr("src","https://raw.githubusercontent.com/muhammederdem/credit-card-form/master/src/assets/images/" + cardType + ".png"); 
            //$('#issuer').html(response[0].issuer.name)
            fragment.appendChild(option);

            for (var i = 0; i < payerCosts.length; i++) {
                fragment.appendChild(new Option(payerCosts[i].recommended_message, payerCosts[i].installments));
            }

            selectorInstallments.appendChild(fragment);
            selectorInstallments.removeAttribute('disabled');
        }
    };
    
    function guessingPaymentMethod(event) {
        var bin = getBin();
    
        if (event.type == "keyup") {
            if (bin.length >= 6) {
                window.Mercadopago.getPaymentMethod({
                    "bin": bin
                }, setPaymentMethodInfo);
            }
        } else {
            setTimeout(function() {
                if (bin.length >= 6) {
                    window.Mercadopago.getPaymentMethod({
                        "bin": bin
                    }, setPaymentMethodInfo);
                }
            }, 100);
        }
    };
    
    function setPaymentMethodInfo(status, response) {
        if (status == 200) {
        const paymentMethodElement = document.querySelector('input[name=paymentMethodId]');
    
        if (paymentMethodElement) {
            paymentMethodElement.value = response[0].id;
        } else {
            const input = document.createElement('input');
            input.setAttribute('name', 'paymentMethodId');
            input.setAttribute('type', 'hidden');
            input.setAttribute('value', response[0].id);     
    
            form.appendChild(input);
        }
    
        Mercadopago.getInstallments({
            "bin": getBin(),
            "amount": parseFloat(document.querySelector('#monto').value),
        }, setInstallmentInfo);
    
        } else {
            //alert(`payment method info error: ${response}`);  
        }
    };

    function setPaymentMethodInfo(status, response) {
        if (status == 200) {
            const paymentMethodElement = document.querySelector('input[name=paymentMethodId]');
        
            if (paymentMethodElement) {
                paymentMethodElement.value = response[0].id;
            } else {
                const input = document.createElement('input');
                input.setAttribute('name', 'paymentMethodId');
                input.setAttribute('type', 'hidden');
                input.setAttribute('value', response[0].id);     
        
                form.appendChild(input);
            }

            Mercadopago.getIssuers(response[0].id, setIssuers);
        
            Mercadopago.getInstallments({
                "bin": getBin(),
                "amount": parseFloat(document.querySelector('#monto').value),
            }, setInstallmentInfo);
        
        } else {
            //alert(`payment method info error: ${response}`);  
        }
    };

    function setIssuers(status, response) {
        if (status == 200) {
            
        } else {
            //alert(`payment method info error: ${response}`);  
        }
    };
  
 });
