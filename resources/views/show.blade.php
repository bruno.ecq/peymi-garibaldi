<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta id="viewport" name="viewport" content ="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Peymi</title>
  <link rel="icon" href="{{ config('peymi.favicon') }}">
  <link rel="stylesheet" href="css/vendor/bootstrap.min.css">
  <link rel="stylesheet" href="css/vendor/bs-stepper.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/index.css">
  <script>
    var mp_public_key = '{{ config('peymi.mp_public_key') }}';
    var url = '{{ config('peymi.global_url') }}';
  </script> 
</head>
<body>
    <div class="loading" style="display: none;"><img src="images/loader.gif" width="100" /></div>
    <div class="container flex-grow-1 flex-shrink-0 py-0 card-form">
        <div class="mb-0 px-4 py-0 bg-white card-form__inner">
          <div class="py-5 text-center logo-title">
            <img class="d-block mx-auto mb-4 image-mob" src="{{ config('peymi.logo') }}" alt="" width="{{ config('peymi.logo_width') }}" height="{{ config('peymi.logo_height') }}">
            <style>
              @media only screen and (max-device-width: 700px){
                .image-mob {
                    width: {{ config('peymi.logo_mobile_width') }} !important;
                    height: {{ config('peymi.logo_mobile_height') }} !important;
                }
              }
            </style>
            <h2>{{ config('peymi.title') }}</h2>
          </div>
          <div id="stepperForm" class="bs-stepper">
            <div class="bs-stepper-header" role="tablist" style="display:none;">
              <div class="step" data-target="#test-form-1">
                <button type="button" class="step-trigger" role="tab" id="stepperFormTrigger1" aria-controls="test-form-1">
                  <span class="bs-stepper-label">DATOS PERSONALES</span>
                  <span class="bs-stepper-circle"></span>
                </button>
              </div>
              <div class="bs-stepper-line"></div>
              <div class="step" data-target="#test-form-2">
                <button type="button" class="step-trigger" role="tab" id="stepperFormTrigger2" aria-controls="test-form-2">
                  <span class="bs-stepper-label">MEDIO DE PAGO</span>
                  <span class="bs-stepper-circle"></span>
                </button>
              </div>
              <div class="bs-stepper-line"></div>
              <div class="step" data-target="#test-form-3">
                <button type="button" class="step-trigger" role="tab" id="stepperFormTrigger3" aria-controls="test-form-3">
                  <span class="bs-stepper-label">CONFIRMACIÓN</span>
                  <span class="bs-stepper-circle"></span>
                </button>
              </div>
            </div>
            <div id="customStepper" class="bs-stepper-header my-4 py-4" role="tablist">
              <div class="wrapper-progressBar">
                <ul class="progressBar">
                  <li>DATOS PERSONALES</li>
                  <li>MEDIO DE PAGO</li>
                  <li>CONFIRMACIÓN</li>
                </ul>
              </div>
            </div>
            <div class="bs-stepper-content">
              <form id="myForm" class="needs-validation" onSubmit="return false" novalidate>
                <div id="test-form-1" role="tabpanel" class="bs-stepper-pane fade" aria-labelledby="stepperFormTrigger1">
                  <div class="form-group card-input">
                    <label for="cardholderName" class="card-input__label">Nombre Completo<span class="text-danger font-weight-bold">*</span></label>
                    <input type="text" id="cardholderName" name="nombre"  data-checkout="cardholderName" class="form-control card-input__input" v-model="cardName" v-on:focus="focusInput" v-on:blur="blurInput" data-ref="cardName" autocomplete="off" placeholder="Nombre completo" required>
                    <div class="invalid-feedback">Por favor ingresa tu nombre completo</div>
                  </div>
                  <div class="form-group card-input">
                    <label for="email" class="card-input__label">Correo electrónico<span class="text-danger font-weight-bold">*</span></label>
                    <input id="email" type="email" name="email" class="form-control card-input__input" placeholder="Correo electrónico" required>
                    <div class="invalid-feedback">Por favor ingresa tu correo electrónico</div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-6 card-input">
                      <label for="codigo" class="card-input__label">Código de Referencia</label>
                      <input id="codigo" type="text" name="codigo" class="form-control card-input__input" placeholder="Código de Referencia">
                      <div class="info">{{ config('peymi.info_codigo') }}</div>
                    </div>
                    <div class="form-group col-md-6 card-input">
                      <label for="factura" class="card-input__label">Nro de factura</label>
                      <input id="factura" type="text" name="factura" class="form-control card-input__input" placeholder="Nro de factura">
                      <div class="info">{{ config('peymi.info_factura') }}</div>
                    </div>
                  </div>
                  <div class="form-group card-input">
                    <label for="monto" class="card-input__label">Monto<span class="text-danger font-weight-bold">*</span></label>
                    <input id="monto" type="number" min="1" name="monto" class="form-control card-input__input" placeholder="Monto" required>
                    <div class="invalid-feedback">Por favor ingresa el monto</div>
                    <div class="info">{{ config('peymi.info_monto') }}</div>
                  </div>
                  <button class="btn btn-primary btn-next-form boton d-block" style="margin: 5px auto;">CONTINUAR</button>
                </div>
                <div id="test-form-2" role="tabpanel" class="bs-stepper-pane fade" aria-labelledby="stepperFormTrigger2">
                  <div class="card-list">
                    <div class="card-item" v-bind:class="{ '-active' : isCardFlipped }">
                      <div class="card-item__side -front">
                        <div class="card-item__focus" v-bind:class="{'-active' : focusElementStyle }" v-bind:style="focusElementStyle" ref="focusElement"></div>
                        <div class="card-item__cover">
                          <img
                          v-bind:src="'https://raw.githubusercontent.com/muhammederdem/credit-card-form/master/src/assets/images/' + currentCardBackground + '.jpeg'" class="card-item__bg">
                        </div>
                        
                        <div class="card-item__wrapper">
                          <div class="card-item__top">
                            <img src="https://raw.githubusercontent.com/muhammederdem/credit-card-form/master/src/assets/images/chip.png" class="card-item__chip">
                            <div class="card-item__type">
                              <transition name="slide-fade-up">
                                <img id="logoTarjeta" v-if="getCardType" v-bind:key="getCardType" alt="" class="card-item__typeImg">
                              </transition>
                            </div>
                          </div>
                          <label for="cardNumber" class="card-item__number" ref="cardNumber">
                            <template v-if="getCardType === 'amex'">
                             <span v-for="(n, $index) in amexCardMask" :key="$index">
                              <transition name="slide-fade-up">
                                <div
                                  class="card-item__numberItem"
                                  v-if="$index > 4 && $index < 14 && cardNumber.length > $index && n.trim() !== ''"
                                >*</div>
                                <div class="card-item__numberItem"
                                  :class="{ '-active' : n.trim() === '' }"
                                  :key="$index" v-else-if="cardNumber.length > $index">
                                  @{{cardNumber[$index]}}
                                </div>
                                <div
                                  class="card-item__numberItem"
                                  :class="{ '-active' : n.trim() === '' }"
                                  v-else
                                  :key="$index + 1"
                                >@{{n}}</div>
                              </transition>
                            </span>
                            </template>
            
                            <template v-else>
                              <span v-for="(n, $index) in otherCardMask" :key="$index">
                                <transition name="slide-fade-up">
                                  <div
                                    class="card-item__numberItem"
                                    v-if="$index > 4 && $index < 15 && cardNumber.length > $index && n.trim() !== ''"
                                  >*</div>
                                  <div class="card-item__numberItem"
                                    :class="{ '-active' : n.trim() === '' }"
                                    :key="$index" v-else-if="cardNumber.length > $index">
                                    @{{cardNumber[$index]}}
                                  </div>
                                  <div
                                    class="card-item__numberItem"
                                    :class="{ '-active' : n.trim() === '' }"
                                    v-else
                                    :key="$index + 1"
                                  >@{{n}}</div>
                                </transition>
                              </span>
                            </template>
                          </label>
                          <div class="card-item__content">
                            <label for="cardName" class="card-item__info" ref="cardName">
                              <div class="card-item__holder">Titular</div>
                              <transition name="slide-fade-up">
                                <div class="card-item__name" v-if="cardName.length" key="1">
                                  <transition-group name="slide-fade-right">
                                    <span class="card-item__nameItem" v-for="(n, $index) in cardName.replace(/\s\s+/g, ' ')" v-if="$index === $index" v-bind:key="$index + 1">@{{n}}</span>
                                  </transition-group>
                                </div>
                                <div class="card-item__name" v-else key="2">Nombre Completo</div>
                              </transition>
                            </label>
                            <div class="card-item__date" ref="cardDate">
                              <label for="cardMonth" class="card-item__dateTitle">Caducidad</label>
                              <label for="cardMonth" class="card-item__dateItem">
                                <transition name="slide-fade-up">
                                  <span v-if="cardMonth" v-bind:key="cardMonth">@{{cardMonth}}</span>
                                  <span v-else key="2">MM</span>
                                </transition>
                              </label>
                              /
                              <label for="cardYear" class="card-item__dateItem">
                                <transition name="slide-fade-up">
                                  <span v-if="cardYear" v-bind:key="cardYear">@{{String(cardYear).slice(2,4)}}</span>
                                  <span v-else key="2">YY</span>
                                </transition>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="card-item__side -back">
                        <div class="card-item__cover">
                          <img
                          v-bind:src="'https://raw.githubusercontent.com/muhammederdem/credit-card-form/master/src/assets/images/' + currentCardBackground + '.jpeg'" class="card-item__bg">
                        </div>
                        <div class="card-item__band"></div>
                        <div class="card-item__cvv">
                            <div class="card-item__cvvTitle">CVV</div>
                            <div class="card-item__cvvBand">
                              <span v-for="(n, $index) in cardCvv" :key="$index">
                                *
                              </span>
            
                          </div>
                            <div class="card-item__type">
                                <img v-bind:src="'https://raw.githubusercontent.com/muhammederdem/credit-card-form/master/src/assets/images/' + getCardType + '.png'" v-if="getCardType" class="card-item__typeImg">
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="mt-4">
                    <div class="form-row">
                      <div class="form-group col-md-6 card-input">
                        <div class="card-input">
                          <label for="cardNumber" class="card-input__label">Número de tarjeta<span class="text-danger font-weight-bold">*</span></label>
                          <input type="text" id="cardNumber" data-checkout="cardNumber" class="form-control card-input__input"  v-mask="generateCardNumberMask" v-model="cardNumber" v-on:focus="focusInput" v-on:blur="blurInput" data-ref="cardNumber" autocomplete="off" required>
                        </div>
                      </div>
                      <div class="form-group col-md-3 card-input">
                        <label for="cardExpirationMonth" class="card-input__label">Fecha de vencimiento</label>
                          <select class="form-control card-input__input -select" id="cardExpirationMonth" data-checkout="cardExpirationMonth"  v-model="cardMonth" v-on:focus="focusInput" v-on:blur="blurInput" data-ref="cardDate" required>
                            <option value="" disabled selected>Mes</option>
                            <option v-bind:value="n < 10 ? '0' + n : n" v-for="n in 12" v-bind:disabled="n < minCardMonth" v-bind:key="n">
                                @{{n < 10 ? '0' + n : n}}
                            </option>
                          </select>
                      </div>
                      <div class="form-group col-md-3 card-input">
                        <label for="cardExpirationYear" class="card-input__label" style="visibility: hidden;">Año</label>
                        <select class="form-control card-input__input -select" data-checkout="cardExpirationYear" id="cardExpirationYear" v-model="cardYear" v-on:focus="focusInput" v-on:blur="blurInput" data-ref="cardDate" required>
                          <option value="" disabled selected>Año</option>
                          <option v-bind:value="$index + minCardYear" v-for="(n, $index) in 12" v-bind:key="n">
                              @{{$index + minCardYear}}
                          </option>
                        </select>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-3 card-input">
                        <div class="card-input">
                          <label for="docType" class="card-input__label">Tipo de Documento</label>
                          <select class="form-control card-input__input -select" id="docType" data-checkout="docType" v-model="cardType" required>
                          </select>
                        </div>
                      </div>
                      <div class="form-group col-md-3 card-input">
                        <div class="card-input">
                          <label for="cardName" class="card-input__label">DNI Titular</label>
                          <input type="text" id="cardName" class="form-control card-input__input" data-checkout="docNumber" v-model="cardNo" autocomplete="off" required>
                        </div>
                      </div>
                      <div class="form-group col-md-6 card-input">
                          <div class="card-input">
                            <label for="cardCvv" class="card-input__label">Código de seguridad</label>
                            <input type="text" class="form-control card-input__input" data-checkout="securityCode" id="cardCvv" v-mask="'####'" maxlength="4" v-model="cardCvv" v-on:focus="flipCard(true)" v-on:blur="flipCard(false)" autocomplete="off" required>
                          </div>
                      </div>
                    </div>  
                    <div class="form-row">
                      <div class="form-group col-md-12 card-input">
                        <label class="sr-only card-input__label" for="installments">Cantidad de cuotas</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <div class="input-group-text" id="issuer"></div>
                          </div>
                          <select id="installments" name="installments" style="background-position: 97.7% center;" class="form-control card-input__input -select" v-model="cardInstallments" placeholder="Cantidad de cuotas" required>
                            <option value="" disabled selected>Cantidad de cuotas</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-6 card-input">
                        <span id="tea" class="float-left" style="font-size: 13px;">CFT_0,00% | TEA_0,00%</span>
                      </div> 
                      <div class="form-group col-md-6 card-input">
                        <span class="float-right total">TOTAL: <span id="montoTotal">----</span></span>
                      </div>
                      </div>
                  </div>
                  <div class="text-center">
                    <button class="btn btn-primary btn-prev-form d-inline-block boton">VOLVER</button>
                    <button id="confirmacion" class="btn btn-primary d-inline-block boton btn-desactivado">CONFIRMAR</button>
                  </div>
                  <input type="hidden" name="amount" id="amount" value="1000"/>
                  <input type="hidden" name="description"/>
                  <input type="hidden" name="paymentMethodId" />
                </div>
                <div id="test-form-3" role="tabpanel" class="bs-stepper-pane fade text-center" aria-labelledby="stepperFormTrigger3">
                  <div class="respuesta">
                      <img id="responseImage" src="images/ok.png"> 
                      <span id="responsePago">PAGO APROBADO/EFECTUADO</span> 
                      <span class="error" style="display:none;">Hubo un error al procesar su pago</span>
                      <span class="error" style="display:none;font-size: 12px;">Estos valores no pudieron ser debitados.<br>Se muestran sólo en carácter informativo.</span>
                        <div class="tabla-wrapper">
                          <table>
                            <tbody>
                              <tr><td style="font-weight: bold;">DATOS DE SU TARJETA</td></tr>
                              <tr><td>Tarjeta <span id="responseMethod">----</span> Finalizada en XXXXXXX-<span id="responseDigits">0000</span></td></tr>
                              <tr><td>&nbsp;</td></tr> 
                              <tr><td style="font-weight: bold;">DETALLE DEL PAGO</td></tr>
                              <tr><td>Importe a abonar: <span id="responseMonto">----</span></td></tr>
                              <tr><td><span id="responseCuotas">X cuotas de XXXXX</span></td></tr>
                              <tr><td>IMPORTE TOTAL: <span id="responseSumaCuotas">XXXX</span></td></tr>
                              <tr><td>&nbsp;</td></tr> 
                              <tr><td>&nbsp;</td></tr>
                              <tr><td>Código de operación: <span id="responseId">00000</span></td></tr>
                              <tr><td>&nbsp;</td></tr> 
                              <tr><td>&nbsp;</td></tr>
                              <tr><td><small>*Este es el importe total abonado, teniendo en cuenta intereses sobre las cuotas</small></td></tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
                  <a class="btn btn-primary boton" href="/">VOLVER AL INICIO</a>
                </div>
              </form>
            </div>
          </div>
          <div class="pt-5 pb-2 text-center footer-respo">
            <img class="mt-3 d-block mx-auto mb-4" src="images/peymi.png" style="width: 100px;">
            <small class="smallMsg"><b><i class="fa fa-lock"></i> Transacciones seguras con HTTP2 + SSL</b></small>
            <div class="my-3" style="border-top: 1px solid var(--gris);">
              <ul class="mt-2 footer-peymi">
                <li><a href="{{ config('peymi.footer1') }}">Términos y condiciones</a></li>
                <li><a href="{{ config('peymi.footer2') }}">Política de privacidad</a></li>
                <li><a href="{{ config('peymi.footer3') }}">Reportar errores</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>


<!-- INICIO Modal Impresión -->
<div class="modal fade" id="print" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document" style="width: 700px;max-width: unset;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">VENTANA EMERGENTE</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="respuesta">
              <div class="tabla-wrapper">
                <div class="py-5 text-center">
                  <img class="d-block mx-auto mb-4" src="{{ config('peymi.logo') }}" alt="" width="72" height="72">
                  <h2>{{ config('peymi.title') }}</h2>
                </div>
                <img id="responseImage" src="images/ok.png">
                <span id="responsePago">PAGO APROBADO/EFECTUADO</span> 
                <span class="error" style="display:none;">Hubo un error al procesar su pago</span>
                <span class="error" style="display:none;font-size: 12px;">Estos valores no pudieron ser debitados.<br>Se muestran sólo en carácter informativo.</span>
                <br>
                <table>
                  <tbody>
                    <tr><td style="font-weight: bold;">DATOS DE SU TARJETA</td></tr>
                    <tr><td>Tarjeta <span id="responseMethod">----</span> Finalizada en XXXXXXX-<span id="responseDigits">0000</span></td></tr>
                    <tr><td>&nbsp;</td></tr> 
                    <tr><td style="font-weight: bold;">DETALLE DEL PAGO</td></tr>
                    <tr><td>Importe a abonar: <span id="responseMonto">----</span></td></tr>
                    <tr><td><span id="responseCuotas">X cuotas de XXXXX</span></td></tr>
                    <tr><td>IMPORTE TOTAL: <span id="responseSumaCuotas">XXXX</span></td></tr>
                    <tr><td>&nbsp;</td></tr> 
                    <tr><td>&nbsp;</td></tr>
                    <tr><td>Código de operación: <span id="responseId">00000</span></td></tr>
                    <tr><td>&nbsp;</td></tr> 
                    <tr><td>&nbsp;</td></tr>
                    <tr><td><small>*Este es el importe total abonado, teniendo en cuenta intereses sobre las cuotas</small></td></tr>
                  </tbody>
                </table>
                <img class="mt-3" src="images/peymi.png" style="width: 100px;">
              </div>
      </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- FIN  Modal Impresión -->
   
<script src="js/vendor/jquery-3.4.1.min.js"></script>
<script src="js/vendor/popper.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/vendor/bs-stepper.min.js" ></script>
<script src="js/vendor/vue.min.js" ></script>
<script src="js/vendor/vue-the-mask.js" ></script>
<script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
<script src="js/stepper.js"></script>
<script src="js/mercadopago.js"></script>
<script src="js/popup.js"></script>
<script src="js/main.js"></script>
</body>
</html>