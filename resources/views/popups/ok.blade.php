  <!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Peymi</title>
  <link rel="stylesheet" href="css/vendor/bootstrap.min.css">
  <link rel="stylesheet" href="css/vendor/bs-stepper.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/index.css">
  <style>
    html{
      height: 100%;
      width: 100%;
    }
    body {
      height: 100%;
      width: 100%;
      background-color: white;
      font-size: 12px;
    }
    .modal-content {
      border: none;
    }
    .modal-header {
      border: none;
      padding: 0;
      padding-top: 1rem;
    }
    .modal-dialog {
      margin: 0 auto;
    }
    .tabla-wrapper {
      margin: 0 auto;
    }
    .h2, h2 {
        font-size: 1rem;
    }
    #responseImage {
        width: 36px;
    }
    .firma {
      display: inline-block;
      text-align: center;
      border-top: 1px solid black;
      margin-left: 5px;
      margin-right: 5px;
    }
    @page { size: landscape; }

    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
  </style>
</head>
<body>
<div class="modal position-relative" id="print" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display:block; overflow: auto;">
<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: unset;">
    <div class="modal-content">
      <div class="modal-body">
            <div class="respuesta">
              <div class="tabla-wrapper">
                <div class="py-2 text-center">
                <img class="d-block mx-auto mb-4" src="{{ config('peymi.logo') }}" alt="" width="{{ config('peymi.logo_popup_width') }}" height="{{ config('peymi.logo_popup_height') }}">
                  <h2>{{ config('peymi.title') }}</h2>
                </div>
                <img id="responseImage" src="{{ $o["src"] }}">
                <span id="responsePago">{{ $o["title"] }}</span> 
                @if ($o["msg"] === 'rejected')
                  <span class="error">Hubo un error al procesar su pago</span>
                  <br>
                  <span class="error" style="font-size: 12px;">Estos valores no pudieron ser debitados.<br>Se muestran sólo en carácter informativo.</span>
                  <br>
                @else
                    
                @endif 
                
                <br>
                <table>
                  <tbody>
                    <tr><td style="font-weight: bold;">DATOS DE SU TARJETA</td></tr>
                    <tr><td>Tarjeta <span id="responseMethod">{{ $o["method"] }}</span> Finalizada en XXXXXXX-<span id="responseDigits">{{ $o["digits"] }}</span></td></tr>
                    <tr><td>&nbsp;</td></tr> 
                    <tr><td style="font-weight: bold;">DETALLE DEL PAGO</td></tr>
                    <tr><td>Importe a abonar: <span id="responseMonto">{{ $o["monto"] }}</span></td></tr>
                    <tr><td><span id="responseCuotas">{{ $o["cuota"] }}</span></td></tr>
                    <tr><td>IMPORTE TOTAL: <span id="responseSumaCuotas">{{ $o["sumaCuotas"] }}</span></td></tr>
                    <tr><td>&nbsp;</td></tr> 
                    <tr><td>&nbsp;</td></tr>
                    <tr><td>Código de operación: <span id="responseId">{{ $o["id"] }}</span></td></tr>
                    <tr><td>&nbsp;</td></tr> 
                    <tr><td>&nbsp;</td></tr>
                    <tr><td><small>*Este es el importe total abonado, teniendo en cuenta intereses sobre las cuotas</small></td></tr>
                  </tbody>
                </table>
                <img class="mt-3" src="images/peymi.png" style="width: 100px;">
                <br>
                <button onclick="window.print()" class="btn btn-primary boton d-inline-block no-print" style="margin: 1rem auto;">IMPRIMIR</button>
                <button onclick="opener.closeModalAndReloadMain()" class="btn btn-primary boton d-inline-block no-print" style="margin: 1rem auto;">VOLVER AL INICIO</button>
              </div>
      </div>
        
      </div>
    </div>
    <div class="modal-content">
      <div class="modal-body">
            <div class="respuesta">
              <div class="tabla-wrapper">
                <div class="py-2 text-center">
                  <img class="d-block mx-auto mb-4" src="{{ config('peymi.logo') }}" alt="" width="{{ config('peymi.logo_popup_width') }}" height="{{ config('peymi.logo_popup_height') }}">
                  <h2>{{ config('peymi.title') }}</h2>
                </div>
                <!--<img id="responseImage" src="{{ $o["src"] }}">
                <span id="responsePago">{{ $o["title"] }}</span> -->
                @if ($o["msg"] === 'rejected')
                  <!--<span class="error">Hubo un error al procesar su pago</span>
                  <br>
                  <span class="error" style="font-size: 12px;">Estos valores no pudieron ser debitados.<br>Se muestran sólo en carácter informativo.</span>
                  <br>-->
                @else
                    
                @endif 
                
                <br>
                <table>
                  <tbody>
                    <tr><td style="font-weight: bold;">DATOS DE SU TARJETA</td></tr>
                    <tr><td>Tarjeta <span id="responseMethod">{{ $o["method"] }}</span> Finalizada en XXXXXXX-<span id="responseDigits">{{ $o["digits"] }}</span></td></tr>
                    <tr><td>&nbsp;</td></tr> 
                    <tr><td style="font-weight: bold;">DETALLE DEL PAGO</td></tr>
                    <tr><td>Importe a abonar: <span id="responseMonto">{{ $o["monto"] }}</span></td></tr>
                    <tr><td><span id="responseCuotas">{{ $o["cuota"] }}</span></td></tr>
                    <tr><td>IMPORTE TOTAL: <span id="responseSumaCuotas">{{ $o["sumaCuotas"] }}</span></td></tr>
                    <tr><td>&nbsp;</td></tr> 
                    <tr><td>Código de operación: <span id="responseId">{{ $o["id"] }}</span></td></tr>
                    <tr><td><small>*Este es el importe total abonado, teniendo en cuenta intereses sobre las cuotas</small></td></tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr><td style="text-align: center;"><div class="col-sm-3 firma">FIRMA</div><div class="col-sm-3 firma">ACLARACIÓN</div><div class="col-sm-3 firma">DNI</div></td></tr>
                  </tbody>
                </table>
                <br>
                <img class="mt-3" src="images/mercadopago.png" style="width: 120px;"><br>
                <button onclick="window.print()" class="btn btn-primary boton d-inline-block no-print" style="margin: 1rem auto; visibility: hidden;">IMPRIMIR</button>
                <button onclick="opener.closeModalAndReloadMain()" class="btn btn-primary boton d-inline-block no-print" style="margin: 1rem auto; visibility: hidden;">VOLVER AL INICIO</button>
              </div>
      </div>
        
      </div>
    </div>
  </div>
</div>
<!-- FIN  Modal Impresión -->
   
<script src="js/vendor/jquery-3.4.1.min.js"></script>
<script src="js/vendor/popper.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
</body>
</html>