<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta id="viewport" name="viewport" content ="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Peymi</title>
  <link rel="icon" href="{{ config('peymi.favicon') }}">
  <link rel="stylesheet" href="css/vendor/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
  <style>
  .table thead th {
	 font-size: 12px;
  }
  .table td {
	 font-size: 12px;
  }
  table.dataTable thead th, table.dataTable thead td {
    font-family: GothamMedium, sans-serif;
    font-weight: 200;
}
  </style>
</head>
<body>
    <div class="loading" style="display: none;"><img src="images/loader.gif" width="100" /></div>
    <div class="container flex-grow-1 flex-shrink-0 py-0 card-form">
        <div class="mb-0 px-4 py-0 bg-white card-form__inner">
          <div class="py-5 text-center logo-title">
            <img class="d-block mx-auto mb-4 image-mob" src="{{ config('peymi.logo') }}" alt="" width="{{ config('peymi.logo_width') }}" height="{{ config('peymi.logo_height') }}">
            <style>
              @media only screen and (max-device-width: 700px){
                .image-mob {
                    width: {{ config('peymi.logo_mobile_width') }} !important;
                    height: {{ config('peymi.logo_mobile_height') }} !important;
                }
              }
            </style>
            <h2>{{ config('peymi.title') }}</h2>
          </div>
          <div id="stepperForm" class="bs-stepper"> 
            <div class="bs-stepper-content">
              <form id="myForm" class="needs-validation" onSubmit="return false" novalidate>
                <div id="test-form-1" role="tabpanel" class="bs-stepper-pane" aria-labelledby="stepperFormTrigger1">
                  <div class="form-group card-input">
                    <label for="fechas" class="card-input__label">Seleccione</label>
                    <select class="form-control card-input__input-select"  id="fechas" v-model="cmbFecha" @change="OnChangeFecha()">
					  <option value="0" >Hoy</option>
					  <option value="1" >Esta semana</option>
					  <option value="2" >Este mes</option>
					  <option value="3" >Este año</option>
					  <option value="4" >Todo</option>
					</select>
                  </div>
				  <div class="form-group card-input">
				  <table id="pagos" class="table table-striped">
					  <thead>
						<th>Fecha</th>
						<th>Nombre y Apellido</th>
						<th>Cod de Ref.</th>
						<th>Núm de FC</th>
						<th>Monto</th>
						<th>Estado</th>
						<th>Cod. Operación</th>
					  </thead>
					  <tbody>
						<!--<tr v-for="o in data" :key="o.id">
							<td>@{{ o.fecha }}</td>
							<td>@{{ o.nombre }}</td>
							<td>@{{ o.codigo }}</td>
							<td>@{{ cut(o.factura) }}</td>
							<td>@{{ o.monto }}</td>
							<td>@{{ o.estado }}</td>
							<td>@{{ o.payment_id }}</td> 
						</tr>-->
					  </tbody>
				  </table>
				  </div>
				  <div class="form-group card-input" style="text-align:center">
				  <label><input type="checkbox" v-model="flagRecarga">&nbsp;&nbsp;Recarga Automáticamente</label>  
				  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="pt-5 pb-2 text-center footer-respo">
            <img class="mt-3 d-block mx-auto mb-4" src="images/peymi.png" style="width: 100px;">
            <small class="smallMsg"><b><i class="fa fa-lock"></i> Transacciones seguras con HTTP2 + SSL</b></small>
            <div class="my-3" style="border-top: 1px solid var(--gris);">
              <ul class="mt-2 footer-peymi">
                <li><a href="{{ config('peymi.footer1') }}">Términos y condiciones</a></li>
                <li><a href="{{ config('peymi.footer2') }}">Política de privacidad</a></li>
                <li><a href="{{ config('peymi.footer3') }}">Reportar errores</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
   
<script src="js/vendor/jquery-3.4.1.min.js"></script>
<script src="js/vendor/popper.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/jquery.dataTables.min.js"></script>
<script src="js/vendor/vue.min.js" ></script>
<script src="js/dashboard.js"></script>
</body>
</html>