new Vue({
  el: "#test-form-1",
  data() {
    return {
      data: [],
	  dataTable:null,
	  cmbFecha: 0,
	  flagRecarga: false,
	  t: null
    };
  },
  mounted() {
	let vm = this
	
	this.dataTable = $('#pagos').DataTable({
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla =(",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			},
			"buttons": {
				"copy": "Copiar",
				"colvis": "Visibilidad"
			}
		}
	});
	vm.OnChangeFecha();
  },
  computed: {
   
  },
  watch: {
    flagRecarga: function(n,o){
		let vm = this;
		if (n) {
			vm.t = setInterval(() => {
				vm.OnChangeFecha();
			}, 1 * 60 * 1000);
		} else {
			clearInterval(vm.t)
		}
	}
  },
  methods: {
	cut(o) {
		if(o.length > 10)
		{
			return o.substring(0,10);
		}
		return o;
    },
	OnChangeFecha() {
		let vm = this;
		if(vm.dataTable){
			vm.dataTable.clear();
			$.get( `/dashboard/data?idx=${vm.cmbFecha}`, function( data ) {
				vm.data = data
				
				vm.data.forEach(o=>{
					vm.dataTable.row.add([
					  o.created_at,
					  o.nombre,
					  o.codigo,
					  vm.cut(o.factura),
					  o.monto,
					  o.estado,
					  o.payment_id
					]).draw(false)
				});
			});
		}
	}
  }
});

function showLoading(){
  $(".loading").css("display","block");
}

function hideLoading(){
  $(".loading").css("display","none");
}