<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ComercioMail extends Mailable
{
    use Queueable, SerializesModels;

    public $cuota;
    public $sumaCuotas;
    public $monto;
    public $method;
    public $title;
    public $src;
    public $id;
    public $digits;
    public $msg;
    public $nombre;
	public $codigo;
	public $factura;

    public function __construct($cuota,$sumaCuotas,$monto,$method,$title,$src,$id,$digits,$msg,$nombre,$codigo,$factura)
    {
        $this->cuota = $cuota;
        $this->sumaCuotas = $sumaCuotas;
        $this->monto = $monto;
        $this->method = $method;
        $this->title = $title;
        $this->src = $src;
        $this->id = $id;
        $this->digits = $digits;
        $this->msg = $msg;
        $this->nombre = $nombre;
		$this->codigo = $codigo;
        $this->factura = $factura;
    }

    public function build()
    {
        return $this->
                markdown('emails.comercio')->
                with([
                    'cuota' => $this->cuota
                ])
                ->subject(config('mail.subject'))
                ;
    }
}
