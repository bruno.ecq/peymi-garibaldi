<?php

return [

    'default' => env('DB_CONNECTION', 'mysql'),

    'connections' => [

        'mysql' => [
            'driver' => 'mysql',
            /*CONFIGURACIONES DE BASE DE DATOS */
            /*
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', 3306),
            'database' => env('DB_DATABASE', 'peymi'),
            'username' => env('DB_USERNAME', 'peymi'),
            'password' => env('DB_PASSWORD', '12345678'),
			*/
            
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', 3306),
            'database' => env('DB_DATABASE', 'peymi_garibaldi'),
            'username' => env('DB_USERNAME', 'peymi_garibaldi'),
            'password' => env('DB_PASSWORD', 'Q4q_v77g'),
            
            /* */
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => env('DB_CHARSET', 'utf8mb4'),
            'collation' => env('DB_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => env('DB_PREFIX', ''),
            'strict' => env('DB_STRICT_MODE', true),
            'engine' => env('DB_ENGINE', null),
            'timezone' => env('DB_TIMEZONE', '+00:00'),
        ],

    ],

    'migrations' => 'migrations',

];
