<?php

namespace App\Http\Middleware;

use Closure;

class HttpBasicAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if($request->getUser() != config('peymi.app_username') || $request->getPassword() != config('peymi.app_password')) {
			$headers = array('WWW-Authenticate' => 'Basic');
			return response('Unauthorized', 401, $headers);
		}

        return $next($request);
    }

}