<?php

return [


    'driver' => env('MAIL_DRIVER', 'smtp'),
    'host' => env('MAIL_HOST', 's7.marketinados.com'),
    'port' => env('MAIL_PORT', 587),
    'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    'username' => env('MAIL_USERNAME','notificaciones@peymi.com.ar'),
    'password' => env('MAIL_PASSWORD','TV2hlGPyeu8S'),
	'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'notificaciones@peymi.com.ar'),
        'name' => env('MAIL_FROM_NAME', 'Peymi - MARKETINADOS'),
    ],
	'subject' => env('MAIL_SUBJECT','Gracias por tu pago, disfrutá tu comida :) - Garibaldi'),
	
	
	/*Se pueden mandar multiples correos separandolos por comas
	*/
	'comercio_from' => env('MAIL_COMERCIO', 'fermmena75@gmail.com'),
	
	'comercio_cc' => env('MAIL_CC_COMERCIO', 'stefyvillon@hotmail.com'),
	'comercio_cco' => env('MAIL_CCO_COMERCIO', 'esteban@marketinados.com'),
	
	'comprador_cc' => env('MAIL_CC_COMPRADOR', 'notificaciones@peymi.com.ar'),
	'comprador_cco' => env('MAIL_CCO_COMPRADOR', 'esteban@marketinados.com'),
/*
    'driver' => env('MAIL_DRIVER', 'smtp'),
    'host' => env('MAIL_HOST', 'smtp.mailtrap.io'),
    'port' => env('MAIL_PORT', 587),
    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'hello@example.com'),
        'name' => env('MAIL_FROM_NAME', 'Example'),
    ],
    'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    'username' => env('MAIL_USERNAME','53d770f8d187d8'),
    'password' => env('MAIL_PASSWORD','af1b302f8b90c0'),
	*/
   

    'sendmail' => '/usr/sbin/sendmail -bs',
    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],
    'log_channel' => env('MAIL_LOG_CHANNEL'),

];
