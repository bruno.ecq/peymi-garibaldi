<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Peymi</title>
</head>
<body style="box-sizing: border-box; margin: 0; font-weight: 400; line-height: 1.5; color: #212529; text-align: left; background-color: #fff; background: #fff; font-family: 'GothamMedium', sans-serif; font-size: 16px;">
<div class="modal show" id="print" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="box-sizing: border-box; /*position: fixed;*/ top: 0; left: 0; z-index: 1050; display: block; width: 100%; height: 100%; overflow: hidden; outline: 0;">
<div class="modal-dialog modal-dialog-centered" role="document" style="box-sizing: border-box; position: relative; pointer-events: none; display: -ms-flexbox; display: flex; -ms-flex-align: center; align-items: center; min-height: calc(100% - 1rem); -webkit-transform: none; transform: none; width: 700px; max-width: unset;padding-right: 17px; display: block;"  aria-modal="true">
    <div class="modal-content" style="box-sizing: border-box; position: relative; display: -ms-flexbox; display: flex; -ms-flex-direction: column; flex-direction: column; width: 100%; pointer-events: auto; background-color: #fff; background-clip: padding-box; border-radius: .3rem; outline: 0;">
      <div class="modal-body" style="box-sizing: border-box; position: relative; -ms-flex: 1 1 auto; flex: 1 1 auto; padding: 1rem;">
            <div class="respuesta" style="box-sizing: border-box; width: 100%; text-align: center;">
              <div class="tabla-wrapper" style="box-sizing: border-box; margin: 0 auto; width: 500px; border: 1px solid #ccc; border-radius: 10px; padding: 15px;">
                <div class="py-5 text-center" style="box-sizing: border-box; padding-top: 0; padding-bottom: 1rem; text-align: center;">
                  <img class="d-block mx-auto mb-4" src="{{ config('peymi.app_url') }}{{ config('peymi.logo') }}" width="{{ config('peymi.logo_width') }}" height="{{ config('peymi.logo_height') }}" style="box-sizing: border-box; vertical-align: middle; border-style: none; display: block; margin-bottom: 0.5rem; margin-right: auto; margin-left: auto;">
                  <h2 style="box-sizing: border-box; margin-top: 0; margin-bottom: .5rem; font-weight: 500; line-height: 1.2; font-size: 2rem;text-align:center;">{{ config('peymi.title') }}</h2>
                </div>
                <img id="responseImage" src="{{ config('peymi.app_url') }}{{ $src }}" style="box-sizing: border-box; vertical-align: middle; border-style: none; width: 48px;" width="48"> 
                <span id="responsePago" style="box-sizing: border-box; display: block; font-weight: bold;text-align:center;">{{ $title }}</span> 
                @if ($msg === 'rejected')
                  <span class="error" style="box-sizing: border-box; display: block;">Hubo un error al procesar su pago</span>
                  <br>
                  <span class="error" style="box-sizing: border-box; display: block; font-size: 12px;">Estos valores no pudieron ser debitados.<br style="box-sizing: border-box;">Se muestran sólo en carácter informativo.</span>
                  <br>
                @else
                    
                @endif 
                <br style="box-sizing: border-box;">
                <table style="box-sizing: border-box; border-collapse: collapse;">
                  <tbody style="box-sizing: border-box; text-align: left;" align="left">
					<tr style="box-sizing: border-box;"><td style="box-sizing: border-box; font-weight: bold;">NOMBRE COMPLETO</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">{{ $nombre }}</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr> 
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box; font-weight: bold;">DATOS DE SU TARJETA</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">Tarjeta <span id="responseMethod" style="box-sizing: border-box;">{{ $method }}</span> Finalizada en XXXXXXX-<span id="responseDigits" style="box-sizing: border-box;">{{ $digits }}</span></td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr> 
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box; font-weight: bold;">DETALLE DEL PAGO</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">Importe a abonar: <span id="responseMonto" style="box-sizing: border-box;">{{ $monto }}</span></td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;"><span id="responseCuotas" style="box-sizing: border-box;">{{ $cuota }}</span></td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">IMPORTE TOTAL: <span id="responseSumaCuotas" style="box-sizing: border-box;">{{ $sumaCuotas }}</span></td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr>
					@if( !empty($codigo) )
					<tr style="box-sizing: border-box;"><td style="box-sizing: border-box; font-weight: bold;">C&Oacute;DIGO DE REFERENCIA</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">{{ $codigo }}</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr>
					@endif
					@if( !empty($factura) )
					<tr style="box-sizing: border-box;"><td style="box-sizing: border-box; font-weight: bold;">N&Uacute;MERO DE FACTURA</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">{{ $factura }}</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr>
					@endif
					<tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">Código de operación: <span id="responseId" style="box-sizing: border-box;">{{ $id }}</span></td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr> 
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;"><small style="box-sizing: border-box; font-size: 80%; font-weight: 400;">*Este es el importe total abonado, teniendo en cuenta intereses sobre las cuotas</small></td></tr>
					<tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr>
                    <tr style="box-sizing: border-box;"><td style="box-sizing: border-box;">&nbsp;</td></tr>
                    <tr style="box-sizing: border-box;">
						<td style="box-sizing: border-box;text-align: center;">
						<div class="col-sm-3" style="display: inline-block;text-align: center;border-top: 1px solid black;margin-left: 5px;margin-right: 5px;">FIRMA</div>
						<div class="col-sm-3" style="display: inline-block;text-align: center;border-top: 1px solid black;margin-left: 5px;margin-right: 5px;">ACLARACIÓN</div>
						<div class="col-sm-3" style="display: inline-block;text-align: center;border-top: 1px solid black;margin-left: 5px;margin-right: 5px;">DNI</div>
						</td>
					</tr>
                  </tbody>
                </table>
                <img class="mt-3" src="{{ config('peymi.app_url') }}images/peymi.png" style="box-sizing: border-box; vertical-align: middle; border-style: none; width: 100px; margin-top: 1rem;" width="100">
              </div>
      </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>