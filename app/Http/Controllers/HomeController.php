<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Mail\PagadorMail;
use App\Mail\ComercioMail;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function __construct()
    {
        \MercadoPago\SDK::setAccessToken( config('peymi.mp_access_token') );
    }

    public function sendMail(Request $r)
    {
		$comercio_cc = explode(',',config('mail.comercio_cc'));
		$comercio_cco = explode(',',config('mail.comercio_cco'));
		$comprador_cc = explode(',',config('mail.comprador_cc'));
		$comprador_cco = explode(',',config('mail.comprador_cco'));
		$comercio_from = explode(',',config('mail.comercio_from'));
		
        Mail::to($r->email)->cc($comprador_cc)->bcc($comprador_cco)->send(new PagadorMail($r->cuota,$r->sumaCuotas,$r->monto,$r->method,$r->title,$r->src,$r->id,$r->digits,$r->msg,$r->codigo,$r->factura));
        Mail::to($comercio_from)->cc($comercio_cc)->bcc($comercio_cco)->send(new ComercioMail($r->cuota,$r->sumaCuotas,$r->monto,$r->method,$r->title,$r->src,$r->id,$r->digits,$r->msg,$r->nombre,$r->codigo,$r->factura));
    } 

    public function popup(Request $r)
    {
        if(empty($r->x))
        {
            return "not valid";
        }
        
        $q = urldecode(base64_decode($r->x));

        parse_str($q,$o);

        return view('popups.ok',[
            "o" => $o
        ]);
    }

    public function show()
    {
        return view('show');
    }
	
	public function dashboard()
    {
        return view('dashboard');
    }
	
	public function getData(Request $r)
    {
		switch ($r->idx) {
			case 0://HOY
				$str = " created_at >= NOW() - INTERVAL 1 DAY ";
				break;
			case 1://ESTA SEMANA
				$str = " created_at >= NOW() - INTERVAL 1 WEEK ";
				break;
			case 2://ESTE MES
				$str = " created_at >= NOW() - INTERVAL 1 MONTH ";
				break;
			case 3://ESTE AÑO
				$str = " created_at >= NOW() - INTERVAL 1 YEAR ";
				break;
			case 4://TODO
				$str = " 1 = 1 ";
				break;
		}
		
		//$users = DB::select("SELECT DATE_FORMAT(created_at,'%Y'), nombre, codigo, factura, monto, estado, payment_id FROM usuarios");
		$users = DB::select("SELECT DATE_FORMAT(created_at,'%d/%m/%Y') created_at, nombre, codigo, factura, monto, estado, payment_id FROM usuarios WHERE " . $str . " ORDER BY CREATED_AT DESC");
		
		return $users;
    }

    public function store(Request $r)
    {        
        $payment = new \MercadoPago\Payment();

        $payment->transaction_amount = $r->monto;
        $payment->token = $r->token;
        $payment->installments = $r->installments;
        $payment->payment_method_id = $r->paymentMethodId;
        $payment->binary_mode = true;
        $payment->payer = array( "email" => $r->email );

        $payment->save();

        /*dd($payment);
        dd($payment->status);*/
        if( is_null($payment->error) ) {
            if( $payment->status == "approved"){
                DB::table('usuarios')->insert(
                    [
                        'nombre' => $r->nombre, 
                        'email' => $r->email,
                        'codigo' => $r->codigo,
                        'factura' => $r->factura,
                        'monto' => $r->monto,
                        'payment_id' => $payment->id,
						'created_at' => new \DateTime()
                    ]
                );
                $arr = array('status' => 'ok', 'msg' => $payment->status, 'id' => $payment->id);
            } else {
                $arr = array('status' => 'ok', 'msg' => $payment->status, 'id' => $payment->id, 'detail' => $payment->status_detail);
            }
        }
        else {
            $arr = array('status' => 'error', 'msg' => $payment->error->message);
        }

        return json_encode($arr);
    }
}
