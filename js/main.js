new Vue({
  el: "#stepperForm",
  data() {
    return {
      currentCardBackground: Math.floor(Math.random()* 25 + 1), 
      cardName: "",
      cardNumber: "",
      cardMonth: "",
      cardYear: "",
      cardType: "",
      cardCvv: "",
      cardNo: "",
      cardInstallments: "",
      minCardYear: new Date().getFullYear(),
      amexCardMask: "#### ###### #####",
      otherCardMask: "#### #### #### ####",
      cardNumberTemp: "",
      isCardFlipped: false,
      focusElementStyle: null,
      isInputFocused: false
    };
  },
  mounted() {
    this.cardNumberTemp = this.otherCardMask;
    document.getElementById("cardNumber").focus();
  },
  computed: {
    getCardType () {
      let number = this.cardNumber;
      let re = new RegExp("^4");
      if (number.match(re) != null) return "visa";

      re = new RegExp("^(34|37)");
      if (number.match(re) != null) return "amex";

      re = new RegExp("^5[1-5]");
      if (number.match(re) != null) return "mastercard";

      re = new RegExp("^6011");
      if (number.match(re) != null) return "discover";
      
      re = new RegExp('^9792')
      if (number.match(re) != null) return 'troy'

      return "visa"; // default type
    },
		generateCardNumberMask () {
			return this.getCardType === "amex" ? this.amexCardMask : this.otherCardMask;
    },
    minCardMonth () {
      if (this.cardYear === this.minCardYear) return new Date().getMonth() + 1;
      return 1;
    }
  },
  watch: {
    cardYear () {
      if (this.cardMonth < this.minCardMonth) {
        this.cardMonth = "";
      }
    }
  },
  methods: {
    flipCard (status) {
      this.isCardFlipped = status;
    },
    focusInput (e) {
      this.isInputFocused = true;
      let targetRef = e.target.dataset.ref;
      let target = this.$refs[targetRef];
      this.focusElementStyle = {
        width: `${target.offsetWidth}px`,
        height: `${target.offsetHeight}px`,
        transform: `translateX(${target.offsetLeft}px) translateY(${target.offsetTop}px)`
      }
    },
    blurInput() {
      let vm = this;
      setTimeout(() => {
        if (!vm.isInputFocused) {
          vm.focusElementStyle = null;
        }
      }, 300);
      vm.isInputFocused = false;
    }
  }
});

$("#confirmacion").click(function() {
  if( flagSubmit ) {
    var $form = document.querySelector('#myForm');
    window.Mercadopago.clearSession();
    window.Mercadopago.createToken($form, sdkResponseHandler); 
    showLoading();
  } else {
    stepperForm.next()
  }
});

function sdkResponseHandler(status, response) {
    if (status != 200 && status != 201) {
        alert("verify filled data");
        //console.log(response.cause[0].description);
        hideLoading();
    }else{
        var form = document.querySelector('#myForm');
        var card = document.createElement('input');
        card.setAttribute('name', 'token');
        card.setAttribute('type', 'hidden');
        card.setAttribute('value', response.id);
        form.appendChild(card);

        const four_digits = response.last_four_digits
        let formData = $('#myForm').serialize();
        $.post(url, formData , function (d) {
            setResultMessage(d.id, four_digits, d.msg, d)
        }, 'json')
        .fail(function(e) {
          //alert( "error" );
          hideLoading();
        });
    }
};


function setResultMessage(id, digits, msg, _o){
  let cuota = selectedInstallment.recommended_message
  let sumaCuotas = cuota.substring(cuota.indexOf("(") + 1,cuota.indexOf(")"))
  let monto =  sumaCuotas.substring(0, sumaCuotas.indexOf(" ")) + ' ' + $("#monto").val()
  let method = installments.payment_method_id
  let nombre = $("#cardholderName").val()
  let title
  let email = $("#email").val()
  let codigo = $("#codigo").val()
  let factura = $("#factura").val()
  let src = ""
  if(msg === "approved")
  {
    title = "PAGO APROBADO/EFECTUADO"
    src = "images/ok.png"
    $(".error").css("display","none");
  }

  if(msg === "rejected")
  {
    title = "PAGO RECHAZADO"
    src = "images/bad.png"
    $(".error").css("display","block");
  }

  $("#responsePago").html(title);
  $("#responseMethod").html(method);
  $("#responseDigits").html(digits);
  $("#responseMonto").html(monto);
  $("#responseCuotas").html(cuota);
  $("#responseSumaCuotas").html(sumaCuotas);
  $("#responseId").html(id);
  $("#responseImage").attr("src",src);
  stepperForm.next()

  if( msg === "approved" ) {

    let q = `cuota=${cuota}&sumaCuotas=${sumaCuotas}&monto=${monto}&method=${method}&title=${title}&src=${src}&id=${id}&digits=${digits}&msg=${msg}&email=${email}&nombre=${nombre}&codigo=${codigo}&factura=${factura}`;
    //http://localhost:8000/email?cuota=1%20de%20$1000&sumaCuotas=$/.10000&monto=$10030&method=VISA&title=PAGO%20APROBADO/EFECTUADO&src=images/ok.png&id=123212&digits=3746&msg=rejected&email=bruno.ecq@gmail.com
  
    $.get(url + `email?${q}` , function (d) {
    }, 'json');
  
    let encodedString = window.btoa(q);
    showModalPopUp(url + `popup?x=${encodedString}`,'Ventana Emergente');
    //http://localhost:8000/popup?x=Y3VvdGE9MSBjdW90YSBkZSAkIDEuMDAwLDAwICgkIDEuMDAwLDAwKSZzdW1hQ3VvdGFzPSQgMS4wMDAsMDAmbW9udG89JCAxMDAwJm1ldGhvZD12aXNhJnRpdGxlPVBBR08gUkVDSEFaQURPJnNyYz1pbWFnZXMvYmFkLnBuZyZpZD0yNDAwMzE1OCZkaWdpdHM9MzcwNCZtc2c9cmVqZWN0ZWQmZW1haWw9YnJ1bm8uZWMycUBnbWFpbC5jb20=
    //showModalPopUp("http://localhost:8000/popup?x=Y3VvdGE9MSBjdW90YSBkZSAkIDEuMDAwLDAwICgkIDEuMDAwLDAwKSZzdW1hQ3VvdGFzPSQgMS4wMDAsMDAmbW9udG89JCAxMDAwJm1ldGhvZD12aXNhJnRpdGxlPVBBR08gUkVDSEFaQURPJnNyYz1pbWFnZXMvYmFkLnBuZyZpZD0yNDAwMzE1OCZkaWdpdHM9MzcwNCZtc2c9cmVqZWN0ZWQmZW1haWw9YnJ1bm8uZWMycUBnbWFpbC5jb20=", "Pagina_CNN");
  
  }
 
  hideLoading();
}


function SegundoFormularioIsValid(){
  var cardNumber = document.getElementById('cardNumber')
  var cardExpirationMonth = document.getElementById('cardExpirationMonth')
  var cardExpirationYear = document.getElementById('cardExpirationYear')
  var docType = document.getElementById('docType')
  var cardName = document.getElementById('cardName')
  var cardCvv = document.getElementById('cardCvv')
  var installments = document.getElementById('installments')

  if(
      (
        !cardNumber.value.length
        || !cardExpirationMonth.value.length
        || !cardExpirationYear.value.length
        || !docType.value.length
        || !cardName.value.length
        || !cardCvv.value.length
        || !installments.value.length
      )
    )
    {
      return false;
    }

    return true;
}

function onChangeSecondForm() {
  if( SegundoFormularioIsValid() )
  {
    $('#confirmacion').removeClass('btn-desactivado');
    flagSubmit = true
  }
  else {
    $('#confirmacion').addClass('btn-desactivado');
    flagSubmit = false
  }
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function closeModalAndReloadMain(){
  closeModalPopUp();
  location.reload(true);
}

$('#myForm').change(function(){
  onChangeSecondForm();
});

$().ready(function(){
  let monto = getParameterByName('costo');
  let cod = getParameterByName('codigo_referencia');

  if(cod){
    $('#codigo').val(cod)
    $('#codigo').attr('readonly','readonly')
  }
  if(monto){
    $('#monto').val(monto)
    $('#monto').attr('readonly','readonly')
  }
});

function showLoading(){
  $(".loading").css("display","block");
}

function hideLoading(){
  $(".loading").css("display","none");
}