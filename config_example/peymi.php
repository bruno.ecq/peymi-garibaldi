<?php
/**
 * @configuraciones de la aplicación
 */

return [

    'logo' => env('LOGO', 'images/logo/logocolor.jpg'),
	'logo_width' => env('LOGO_WIDTH', '350'),
    'logo_height' => env('LOGO_HEIGHT', ''),
	
	'logo_mobile_width' => env('LOGO_MOB_WIDTH', '200px'),
    'logo_mobile_height' => env('LOGO_MOB_HEIGHT', ''),
	
	'logo_popup_width' => env('LOGO_POPUP_WIDTH', '250'),
    'logo_popup_height' => env('LOGO_POPUP_HEIGHT', ''),
	
    'title' => env('TITLE', ''),
    
	'mp_public_key' => env('MP_PUBLIC_KEY', 'APP_USR-e6b9b053-3f82-4a40-a0fd-136ba1e7e72d'),
    'mp_access_token' => env('MP_ACCESS_TOKEN', 'APP_USR-3386248121769206-032620-4aa67c4c77c0172c4fbe7ef7d2bfad87-143306510'),
	
	/*
	'mp_public_key' => env('MP_PUBLIC_KEY', 'TEST-dd1b6ac6-b638-4494-8a68-185e3dca5aa6'),
    'mp_access_token' => env('MP_ACCESS_TOKEN', 'TEST-493564351199903-022022-b2a1a4155046b9d7dd6a48c897bda322-84992355'),
	*/
    'app_url' => env('GLOBAL_URL', 'https://garibaldi.peymi.com.ar/'),
	
	'favicon' => env('FAVICON', 'images/favicon/favicon_cflgs.ico'),
    'footer1' => env('FOOTER1', 'https://marketinados.com/terminos-y-condiciones-payme-de-marketinados/'),
    'footer2' => env('FOOTER2', 'https://marketinados.com/politica-de-privacidad/'),
    'footer3' => env('FOOTER3', 'https://marketinados.com/reportar-un-error-en-peymi/'),

    'info_monto' => env('INFO_MONTO', '***Colocar el decimal con puntos separador'),
	'info_codigo' => env('INFO_CODIGO', '*Este campo no es obligatorio'),
    'info_factura' => env('INFO_FACTURA', '*Este campo no es obligatorio'),
	/*
    'logo' => env('LOGO', 'images/logo/bootstrap-solid.svg'),
    'title' => env('TITLE', 'MI EMPRESA'),
    'mp_public_key' => env('MP_PUBLIC_KEY', 'TEST-dd1b6ac6-b638-4494-8a68-185e3dca5aa6'),
    'mp_access_token' => env('MP_ACCESS_TOKEN', 'TEST-493564351199903-022022-b2a1a4155046b9d7dd6a48c897bda322-84992355'),
    'app_url' => env('APP_URL', 'http://marketinados.peymi.com.ar/'),
	*/
	
		'app_username' => env('APP_USERNAME', 'garibaldi'),
	'app_password' => env('APP_PASSWORD', 'gari8ald1'),
];