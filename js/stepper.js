
var stepperForm

function RemoveClass(element,idx) {
  for (let index = element.length - 1; index >= 0; index--) {
    const e = $(element[index]);
    e.removeClass('active');
    if (index == idx && idx != -1) {
      return false;
    }
  }
}

function AddClass(element,idx){
  for (let index = 0; index < element.length; index++) {
    const e = $(element[index]);
    e.addClass('active');
    if (index == idx && idx != -1) {
      return false;
    }
  }
}

document.addEventListener('DOMContentLoaded', function () {
  var stepperFormEl = document.querySelector('#stepperForm')

  stepperForm = new Stepper(stepperFormEl, {
    animation: true
  })

  var btnNextList = [].slice.call(document.querySelectorAll('.btn-next-form'))
  var btnPrevList = [].slice.call(document.querySelectorAll('.btn-prev-form'))
  var stepperPanList = [].slice.call(stepperFormEl.querySelectorAll('.bs-stepper-pane'))
  var form = stepperFormEl.querySelector('.bs-stepper-content form')
  /*FORM 1*/
  var cardholderName = document.getElementById('cardholderName')
  var email = document.getElementById('email')
  var codigo = document.getElementById('codigo')
  var factura = document.getElementById('factura')
  var monto = document.getElementById('monto')
  /*END FORM 1*/
  /*FORM 2*/
  var cardNumber = document.getElementById('cardNumber')
  var cardExpirationMonth = document.getElementById('cardExpirationMonth')
  var cardExpirationYear = document.getElementById('cardExpirationYear')
  var docType = document.getElementById('docType')
  var cardName = document.getElementById('cardName')
  var cardCvv = document.getElementById('cardCvv')
  var installments = document.getElementById('installments')
  /*END FORM 2*/

  btnPrevList.forEach(function (btn) {
    btn.addEventListener('click', function () {
      stepperForm.previous()
    })
  })

  btnNextList.forEach(function (btn) {
    btn.addEventListener('click', function () {
      stepperForm.next()
    })
  })

  stepperFormEl.addEventListener('show.bs-stepper', function (event) {
    form.classList.remove('was-validated')
    var nextStep = event.detail.indexStep
    var currentStep = nextStep

    if (currentStep > 0) {
      currentStep--
    }

    var stepperPan = stepperPanList[currentStep]

    /*if ((stepperPan.getAttribute('id') === 'test-form-1' && !inputMailForm.value.length)
    || (stepperPan.getAttribute('id') === 'test-form-2' && !inputPasswordForm.value.length)) 
    {
      event.preventDefault()
      form.classList.add('was-validated')
    }*/
    if 
    (
      ( 
        stepperPan.getAttribute('id') === 'test-form-1' 
        && 
        (
          !cardholderName.value.length 
          || !email.value.length 
          //|| !codigo.value.length 
          //|| !factura.value.length 
          || !monto.value.length
        )
      )
      ||
      (
        stepperPan.getAttribute('id') === 'test-form-2' 
        && 
        (
          !cardNumber.value.length
          || !cardExpirationMonth.value.length
          || !cardExpirationYear.value.length
          || !docType.value.length
          || !cardName.value.length
          || !cardCvv.value.length
          || !installments.value.length
        )
        && !flagSubmit
      )
    )
    {
      event.preventDefault()
      form.classList.add('was-validated')
    }
  })

  stepperFormEl.addEventListener('shown.bs-stepper', function (event) {
    var nextStep = event.detail.indexStep
    var currentStep = nextStep
    var actives = $('#customStepper .active').length
    if( actives != 0 ) {
      if( actives - 1 != currentStep ) {
        RemoveClass($('.progressBar li'),currentStep);
      }
    }
    AddClass($('.progressBar li'), nextStep);
  })
})